package configFactory;

import by.javacourse.configurations.Configuration;
import by.javacourse.configurations.PropertyConfiguration;
import by.javacourse.factories.ConfigFactory;

public class PropertyConfigFactory implements ConfigFactory {

    @Override
    public Configuration getConfiguration() throws Exception {
        return new PropertyConfiguration();
    }
}
