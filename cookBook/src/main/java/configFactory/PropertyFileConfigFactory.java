package configFactory;

import by.javacourse.configurations.Configuration;
import by.javacourse.configurations.PropertyConfiguration;
import by.javacourse.factories.ConfigFactory;

import java.io.IOException;

/**
 * Created by puzevich on 08.11.16.
 */
public class PropertyFileConfigFactory implements ConfigFactory {
    private final static String FILE_PATH = "cookBook.properties";
    @Override
    public Configuration getConfiguration() throws IOException {
        return new PropertyConfiguration(FILE_PATH);
    }
}
