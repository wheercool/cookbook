package configFactory;

import by.javacourse.configurations.Configuration;
import by.javacourse.configurations.CopmpositeConfiguration;
import by.javacourse.factories.ConfigFactory;

import java.util.ArrayList;

/**
 * Created by puzevich on 15.11.16.
 */
public class CompositeConfigFactory implements ConfigFactory {

    private ArrayList<ConfigFactory> configFactories = new ArrayList<>();
    @Override
    public Configuration getConfiguration() throws Exception {
        CopmpositeConfiguration config = new CopmpositeConfiguration();
        for (ConfigFactory configFactory : configFactories) {
            config.add(configFactory.getConfiguration());
        }
        return config;
    }

    public void add (ConfigFactory configFactory) {
        configFactories.add(configFactory);
    }
}
