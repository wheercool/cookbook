package configFactory;

import by.javacourse.configurations.Configuration;
import by.javacourse.configurations.SpringConfiguration;
import by.javacourse.factories.ConfigFactory;

public class SpringConfigFactory implements ConfigFactory {
    @Override
    public Configuration getConfiguration() throws Exception {
        return new SpringConfiguration();
    }
}
