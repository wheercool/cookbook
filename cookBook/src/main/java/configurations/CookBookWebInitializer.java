package configurations;

import by.javacourse.controller.FrontController;
import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class CookBookWebInitializer implements WebApplicationInitializer {
    private static Logger log = Logger.getLogger(CookBookWebInitializer.class);

@Override
public void onStartup(ServletContext servletContext) throws ServletException {
    WebApplicationContext context = getXMLContext();
    servletContext.addListener(new ContextLoaderListener(context));
    servletContext.setInitParameter("configFactory", "configFactory.SpringConfigFactory");
    FrontController frontController = new FrontController();
    ServletRegistration.Dynamic dispatcher = servletContext.addServlet("frontController", frontController); //как сделать его бином, если я его создаю с помощью new
    frontController.setApplicationContext(context);
    //servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
    dispatcher.setLoadOnStartup(1);
    dispatcher.addMapping("");
}

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(WebAppConfig.class);
        log.info(context.getClass()+"!!!!!!!!!!!!!!create root web application context");
        return context;
    }

    private XmlWebApplicationContext getXMLContext() {
        XmlWebApplicationContext context = new XmlWebApplicationContext();
        context.setConfigLocation("/WEB-INF/cookBook-context.xml");
        //context.register(WebAppConfig.class);
        log.info(context.getClass()+"!!!!!!!!!!!!!!create root web application context");
        return context;
    }

}
