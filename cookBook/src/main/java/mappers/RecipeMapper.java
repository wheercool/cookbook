package mappers;

import recipe.CreateRecipeViewModel;
import recipe.Ingridient;
import recipe.Recipe;
import recipe.Step;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecipeMapper {
    private final static String FIELD_NAME = "name";
    private final static String FIELD_URL = "url";
    private final static String FIELD_SHORT_DESCRIPTION = "shortDescription";
    private final static String FIELD_ID = "id";
    private final static String FIELD_AMOUNT_OF_PERSON = "AmountOfPerson";
    private final static String FIELD_STEPS = "steps";
    private final static String FIELD_INGRIDIENTS = "ingridients";
    private final static String FIELD_ADD_STEP = "addStep";
    private final static String FIELD_ADD_INGRIDIENT = "addIngridient";
    private Map<String, String []> parametrs;

    public void setMap(Map<String, String[]> parametrs) {
        this.parametrs = parametrs;
    }

    public Recipe mapToRecipe(){
        Recipe recipe = new Recipe();
        recipe.setName(getStringValueFromMap( FIELD_NAME ));
        recipe.setUrl(getStringValueFromMap( FIELD_URL ));
        recipe.setShortDescription(getStringValueFromMap( FIELD_SHORT_DESCRIPTION ));
        if(getIntegerValueFromMap( FIELD_ID ) != null) {
            recipe.setId(getIntegerValueFromMap( FIELD_ID ));
        }
        recipe.setAmountOfPerson(getIntegerValueFromMap( FIELD_AMOUNT_OF_PERSON ));


        //get List<Ingridient>
        List<Step> steps = new ArrayList<>();
        Step step = getAddtStep();
        steps.add(step);
        recipe.setSteps(steps);
        return recipe;
    }

    public CreateRecipeViewModel mapToRecipeVewModel() {
        CreateRecipeViewModel recipeViewModel = new CreateRecipeViewModel();
        recipeViewModel.setName(getStringValueFromMap( FIELD_NAME ));
        recipeViewModel.setUrl(getStringValueFromMap( FIELD_URL ));
        recipeViewModel.setShortDescription(getStringValueFromMap( FIELD_SHORT_DESCRIPTION ));
        if(getIntegerValueFromMap( FIELD_ID ) != null) {
            recipeViewModel.setId(getIntegerValueFromMap( FIELD_ID ));
        }
        recipeViewModel.setAmountOfPerson(getIntegerValueFromMap( FIELD_AMOUNT_OF_PERSON ));
        //set addFields
        recipeViewModel.setAddStep(getAddtStep());
        recipeViewModel.setAddIngridient(getIngridient());
        //get List<Ingridient>
        List<Ingridient> ingridients = new ArrayList<>();
        List<Step> steps = new ArrayList<>();

        recipeViewModel.setSteps(steps);
        return recipeViewModel;
    }
    private String getStringValueFromMap ( String key ) {
        return (parametrs.containsKey(key)) ? parametrs.get(key)[0] : null;
    }

    private Integer getIntegerValueFromMap ( String key ) {
        return (parametrs.containsKey(key)) ? Integer.valueOf(parametrs.get(key)[0]) : null;
    }

    public Step getAddtStep(){
        Step step = new Step();
        step.setName(getStringValueFromMap("addStep[name]"));
        step.setDescription(getStringValueFromMap("addStep[description]"));
        step.setLogo(getStringValueFromMap("addStep[logo]"));
        return step;
    }

    public Ingridient getIngridient () {
        Ingridient ingridient = new Ingridient();
        ingridient.setName(getStringValueFromMap("addIngridient[name]"));
        ingridient.setMeasure(getStringValueFromMap("addIngridient[measure]"));
        ingridient.setQuantity(getIntegerValueFromMap( "addIngridient[quantity]"));
        return ingridient;
    }

    public Step getStep(String nameStep, int index){
        Step step = new Step();
        step.setName(getStringValueFromMap(nameStep + "[name]"));
        step.setDescription(getStringValueFromMap(nameStep + "[description]"));
        step.setLogo(getStringValueFromMap(nameStep + "[logo]"));
        return step;
    }
}
