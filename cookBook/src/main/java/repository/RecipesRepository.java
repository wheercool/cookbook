package repository;

import recipe.Recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by puzevich on 30.09.16.
 */
public class RecipesRepository implements Repository {
    private static  Map<Integer,Recipe> repository = new HashMap<Integer, Recipe>();
    private  Integer size=0;


    @Override
    public void addRecipe(Recipe recipe){
        size++;
        recipe.setId(size);
        repository.put(size, recipe);
    }

    @Override
    public Recipe getRecipebyId(Integer id){
        return repository.get(id);
    }

    @Override
    public List<Recipe> getAllRecipes() {
        List<Recipe> list = new ArrayList<Recipe>();
        for(int i = 1; i <= size; i++) {
            list.add(repository.get(i));
        }
        return list;
    }

    @Override
    public void remove(Integer id) {
        repository.get(id);
    }
}
