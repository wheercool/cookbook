package repository;

import recipe.Recipe;

import java.util.List;

/**
 * Created by puzevich on 16.11.16.
 */
public interface Repository {
    void addRecipe(Recipe recipe);

    Recipe getRecipebyId(Integer id);

    List<Recipe> getAllRecipes();

    void remove(Integer id);
}
