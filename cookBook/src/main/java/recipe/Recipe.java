package recipe;

import java.util.List;

public class Recipe {
    private Integer id;
    private String name;
    private String url;//pictures
    private String shortDescription;
    private Integer amountOfPerson;
    private List<Ingridient> ingridients;
    private List<Step> steps;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Integer getAmountOfPerson() {
        return amountOfPerson;
    }

    public void setAmountOfPerson(Integer amountOfPerson) {
        this.amountOfPerson = amountOfPerson;
    }

    public List<Ingridient> getIngridients() {
        return ingridients;
    }

    public void setIngridients(List<Ingridient> ingridients) {
        this.ingridients = ingridients;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (id != null ? !id.equals(recipe.id) : recipe.id != null) return false;
        if (name != null ? !name.equals(recipe.name) : recipe.name != null) return false;
        if (url != null ? !url.equals(recipe.url) : recipe.url != null) return false;
        if (shortDescription != null ? !shortDescription.equals(recipe.shortDescription) : recipe.shortDescription != null)
            return false;
        if (amountOfPerson != null ? !amountOfPerson.equals(recipe.amountOfPerson) : recipe.amountOfPerson != null)
            return false;
        if (ingridients != null ? !ingridients.equals(recipe.ingridients) : recipe.ingridients != null) return false;
        return steps != null ? steps.equals(recipe.steps) : recipe.steps == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        result = 31 * result + (amountOfPerson != null ? amountOfPerson.hashCode() : 0);
        result = 31 * result + (ingridients != null ? ingridients.hashCode() : 0);
        result = 31 * result + (steps != null ? steps.hashCode() : 0);
        return result;
    }
}