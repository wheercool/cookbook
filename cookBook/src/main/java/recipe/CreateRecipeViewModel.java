package recipe;

public class CreateRecipeViewModel extends Recipe {

    Step addStep = new Step();
    Ingridient addIngridient = new Ingridient();

    public Step getAddStep() {
        return addStep;
    }

    public void setAddStep(Step addStep) {
        this.addStep = addStep;
    }

    public Ingridient getAddIngridient() {
        return addIngridient;
    }

    public void setAddIngridient(Ingridient addIngridient) {
        this.addIngridient = addIngridient;
    }

}
