package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import by.javacourse.commands.ViewTarget;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import recipe.CreateRecipeViewModel;

import java.util.Map;

@Component
public class CreateRecipeCommand implements Command {
    private static Logger log = Logger.getLogger(CreateRecipeCommand.class);
    public Target process(Map<String, String[]> parametrs) {
        log.info("Create Recipe. Create RecipeViewModel");
        CreateRecipeViewModel viewModel = new CreateRecipeViewModel();
        return new ViewTarget("/add.jsp", viewModel);
    }
}
