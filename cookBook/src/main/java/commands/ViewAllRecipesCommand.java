package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import by.javacourse.commands.ViewTarget;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import recipe.Recipe;
import repository.RecipesRepository;
import repository.Repository;

import java.util.List;
import java.util.Map;


@Component
//@Qualifier("viewAllRecipes")
public class ViewAllRecipesCommand implements Command {
    private static Logger log = Logger.getLogger(ViewAllRecipesCommand.class);

    private Repository repository;

    public ViewAllRecipesCommand(Repository repository) {
        this.repository = repository;
    }

    public ViewAllRecipesCommand() {
        repository = new RecipesRepository();
    }

    public Target process(Map<String, String[]> parametrs) {
        log.info("ViewAllRecipesCommand");
        List<Recipe> recipes = repository.getAllRecipes();
        return new ViewTarget("/list.jsp", recipes);
    }
}
