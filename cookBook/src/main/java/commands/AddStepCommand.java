package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import by.javacourse.commands.ViewTarget;
import recipe.CreateRecipeViewModel;
import recipe.Step;
import mappers.RecipeMapper;

import java.util.Map;

/**
 * Created by wheercool on 05.10.2016.
 */
public class AddStepCommand implements Command {
    private RecipeMapper recipeMapper = new RecipeMapper();
    public Target process(Map<String, String[]> parametrs) {
        CreateRecipeViewModel viewModel = new CreateRecipeViewModel();
        //get ViewModel
        recipeMapper.setMap(parametrs);
        viewModel = recipeMapper.mapToRecipeVewModel();
        //change ViewModel : get addStep and add Step to ViewModel
        Step addStep = viewModel.getAddStep();
        viewModel.getSteps().add(addStep);
        viewModel.setAddStep(null);
        return new ViewTarget("/add.jsp", viewModel);
    }
}
