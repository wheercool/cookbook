package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import by.javacourse.commands.ViewTarget;
import recipe.CreateRecipeViewModel;
import recipe.Step;
import mappers.RecipeMapper;

import java.util.Map;

public class AddIngridientCommand implements Command {
    private RecipeMapper recipeMapper = new RecipeMapper();
    @Override
    public Target process(Map<String, String[]> parametrs) {
        CreateRecipeViewModel viewModel = new CreateRecipeViewModel();
        //get ViewModel
        recipeMapper.setMap(parametrs);
        viewModel = recipeMapper.mapToRecipeVewModel();
        //get addStep and add Step to ViewModel
        Step addStep = recipeMapper.getAddtStep();
        viewModel.getSteps().add(addStep);
        //do action
        return new ViewTarget("/add.jsp", viewModel);
    }
}
