package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import by.javacourse.commands.ViewTarget;
import mappers.RecipeMapper;
import org.apache.log4j.Logger;
import recipe.CreateRecipeViewModel;
import recipe.Recipe;
import repository.RecipesRepository;
import repository.Repository;

import java.util.Map;

public class AddRecipeCommand implements Command {
    private static Logger log = Logger.getLogger(AddRecipeCommand.class);
    private Repository repository;
    private RecipeMapper recipeMapper;


    public AddRecipeCommand (Repository repository) {
        this.repository = repository;
        recipeMapper = new RecipeMapper();
    }

    public AddRecipeCommand (RecipeMapper recipeMapper, Repository repository) {
        this.recipeMapper = recipeMapper;
        this.repository = repository;
    }

    public AddRecipeCommand (){
        this.repository = new RecipesRepository();
        this.recipeMapper = new RecipeMapper();
    }

    public Target process(Map<String, String[]> parametrs) {
        //create view model
        CreateRecipeViewModel viewModel = new CreateRecipeViewModel();
        recipeMapper.setMap(parametrs);

        Recipe recipe = recipeMapper.mapToRecipe();
        //add recipe to repository
        repository.addRecipe(recipe);
        log.info("Add recipe to repository");
        return new ViewTarget("/view.jsp", recipe);
    }


}
