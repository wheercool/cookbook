package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.Target;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by puzevich on 30.09.16.
 */
public class ViewRecipeCommand implements Command {

    private static Logger log = Logger.getLogger(ViewRecipeCommand.class);

    public Target process(Map<String, String[]> parametrs) {
        log.info("ViewRecipeCommand");
        //RecipesRepository.getRecipesRepository().getRecipebyId(1);
        return new Target("/view.jsp");
    }
}
