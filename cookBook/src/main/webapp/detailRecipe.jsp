<%--
  Created by IntelliJ IDEA.
  User: puzevich
  Date: 29.09.16
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View</title>
</head>
<body>

<jsp:useBean id="model" class="recipe.Recipe" scope="request" />

<h1>View Page</h1>
<table>
    <tbody>
        <tr>
            <td>Name</td>
            <td><%= model.getName() %></td>
        </tr>

        <tr>
            <td>Author</td>
            <td><%= model.getAuthor() %></td>
        </tr>
    </tbody>
</table>

</body>
</html>
