<%--
  Created by IntelliJ IDEA.
  User: puzevich
  Date: 30.09.16
  Time: 09:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>List of Recipes</title>
</head>
<body>
<table>
    <%--<jsp:useBean id="model" type="java.util.List<recipe.Recipe>" scope="request" />--%>
    <c:out value="${param.size()}" />
    <tr>
        <th>Name</th>
    </tr>

    <c:forEach var="recipe" items="${model}" varStatus="i" >
        <tr><td><c:out value="${recipe.getName()}" /></td></tr>
    </c:forEach>
</table>

</body>
</html>
