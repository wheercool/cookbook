<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Создание рецепта</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style>
        .steps {
            /*list-style: none;*/
        }
        .step {
            list-style-type: none;
        }

        .ingridient {
            list-style-type: none;
        }
    </style>
    <script>
        function runCommand(value) {
            var command = document.getElementById('command');
            command.setAttribute('value', value);
            var form = document.getElementById('form');
            form.submit();
        }

        function remove(command, idName, id) {
            var form = document.getElementById('form');
            var input = document.createElement('input')
            input.setAttribute('type', 'hidden')
            input.setAttribute('value', id)
            input.setAttribute('name', idName)
            form.appendChild(input);
            runCommand(command, id);
        }

        removeStep = remove.bind(null, 'removeStep', 'step_id')

        removeIngiridient = remove.bind(null, 'removeIngiridient', 'ingridient_id')

    </script>
</head>
<jsp:useBean id="model" class="recipe.CreateRecipeViewModel" scope="request"/>

<body>
<div class="panel panel-primary main">
    <div class="panel-heading"><h3>Создание рецепта</h3></div>
    <form method="post" action="/cookBook/" class="form-horizontal" id="form">
        <input type="hidden" id="command" name="command" />
        <!-- Метаданные рецепта -->
        <div class="panel-body">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Название блюда</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" value="${model.name}">
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Картинка</label>
                    <div class="col-sm-10">

                        <div class="col-sm-12">
                            <img src="no-image-box.png" alt="">
                        </div>

                        <div class="col-sm-12">
                            <!-- <div class="col-sm-2"> -->
                            <div class="form-group">
                                <input name="logo" class="form-control-file" type="file">
                            </div>
                        </div>
                        <!-- </div>  -->
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Краткое описание</label>
                    <div class="col-sm-10">
                        <textarea name="description" class="form-control" rows="4"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Время приготовления (мин)</label>
                    <div class="col-sm-8">
                        <input name="time" type="number" class="form-control " value="${model.time}">
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="" class="col-sm-4 control-label">Количество персон</label>
                    <div class="col-sm-8">
                        <input name="amountOfPerson" type="number" class="form-control " value="${model.amountOfPerson}">
                    </div>
                </div>
            </div>
        </div>

        <!-- ШАГИ -->
        <div class="steps panel panel-success">
            <div class="panel-heading">
                <h4>Шаги</h4>
            </div>

            <div class="panel-body">
                <div class="panel panel-default">
                    <!-- Форма добавления шага -->
                    <div class="panel-body add-step">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Название</label>
                            <div class="col-sm-10">
                                <input name="addStep[name]" type="text" class="form-control" value="${model.addStep.name}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Картинка</label>
                            <div class="col-sm-10">
                                <div class="col-sm-6">
                                    <img src="${model.addStep.logo}" alt="">
                                </div>

                                <div class="col-sm-12">
                                    <!-- <div class="col-sm-2"> -->
                                    <div class="form-group">
                                        <input name="addStep[logo]" class="form-control-file" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Описание</label>
                            <div class="col-sm-10">
                                <textarea name="addStep[description]" class="form-control" rows="4">${model.addStep.description}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button class="btn btn-success pull-right" onclick="runCommand('addStep')">Добавить шаг</button>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <!-- Созданные шаги -->`
                <c:forEach var="item" items="${model.steps}" varStatus="myIndex">
                    <div class="col-lg-6">`
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Название</label>
                                    <div class="col-sm-10">
                                        <input name="steps[name][myIndex]" type="text" class="form-control" value="Шаг 1">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Картинка</label>
                                    <div class="col-sm-10">
                                        <div class="col-sm-12">
                                            <img src="no-image-box.png" alt="">
                                        </div>

                                        <div class="col-sm-12">
                                            <!-- <div class="col-sm-2"> -->
                                            <div class="form-group">
                                                <input name="steps[logo][]" class="form-control-file" type="file">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Описание</label>
                                    <div class="col-sm-10">
                                        <textarea name="steps[description][]" class="form-control" rows="4">Поджарить морковку с луком до золотистого цвета</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <button class="btn btn-danger pull-right" onclick="removeStep(${myIndex.index})">Удалить шаг</button>
                                <div class="clearfix"></div>
                            </div>

                        </div>

                    </div>
                </c:forEach>
            </div>
        </div>

        <!-- Ингридиенты -->
        <div class="panel panel-warning ingridients">
            <div class="panel-heading">
                <h4>Состав</h4>
            </div>

            <div class="panel-body">
                <!-- Форма добавления ингридиента -->
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Ингридиент</label>
                            <div class="col-sm-10">
                                <input name="addIngridient[name]" type="text" class="form-control ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Количество</label>
                            <div class="col-sm-10">
                                <input name="addIngridient[quantity]" type="number" class="form-control ">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Единицы</label>
                            <div class="col-sm-10">
                                <input name="addIngridient[meausure]"  type="text" class="form-control ">
                            </div>
                        </div>

                    </div>


                    <div class="panel-footer">
                        <button class="btn btn-success pull-right" onclick="runCommand('addIngridient')">Добавить ингридиент</button>
                        <div class="clearfix"></div>
                    </div>



                </div>
                <c:forEach var="item" items="${model.ingridients}">
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Ингридиент</label>
                                        <div class="col-sm-8">

                                            <input type="text" class="form-control" value="${model.ingridients.get(item).name}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Количество</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" value="${model.ingridients.get(item).quantity}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-4 control-label">Единицы</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="${model.ingridients.get(item).measure}">
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-danger pull-right" onclick="removeIngiridient(1)">Удалить ингридиент</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                </c:forEach>

            </div>
        </div>

        <div class="panel-footer">
            <div class="btn btn-primary pull-right" onclick="runCommand('addRecipe')">Сохранить рецепт</div>

            <div class="clearfix"></div>
        </div>
    </form>
</div>

</body>
</html>