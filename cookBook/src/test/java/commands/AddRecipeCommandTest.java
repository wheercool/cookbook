package commands;

import by.javacourse.commands.ViewTarget;
import mappers.RecipeMapper;
import org.junit.Before;
import org.junit.Test;
import recipe.Recipe;
import repository.Repository;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AddRecipeCommandTest {
    private Map<String, String[]> parametrs;
    @Before
    public void prepareParametrsMap (){
        parametrs = new HashMap<>();
        parametrs.put("id", new String[]{"1"});
        parametrs.put("name", new String[]{"test"});
        parametrs.put("shortDescription", new String[]{"test"});
        parametrs.put("amountOfperson", new String[]{"1"});
    }

    @Test
    public void shouldCallAddRecipeInRepository() {
        Repository repository = mock(Repository.class);
        AddRecipeCommand command = new AddRecipeCommand(repository);
        command.process(anyMap());
        verify(repository).addRecipe(anyObject());
    }


    @Test
    public void shouldReturnCorectlyFillingRecipeFromRecievedParametrs () {
        Repository repo = mock(Repository.class);
        RecipeMapper recipeMapper = new RecipeMapper();
        recipeMapper.setMap(parametrs);
        Recipe expectedRecipe = recipeMapper.mapToRecipe();
        AddRecipeCommand command = new AddRecipeCommand(recipeMapper, repo);
        Recipe recipe = (Recipe) ( (ViewTarget) command.process(parametrs)).getParam();
        //veriry that return ViewTarget with ViewModel filling expected Recipe
        assertThat(recipe).isEqualTo(expectedRecipe);
    }
}
