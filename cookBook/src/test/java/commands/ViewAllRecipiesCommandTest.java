package commands;

import by.javacourse.commands.Command;
import org.junit.Test;
import repository.Repository;

import java.util.HashMap;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

public class ViewAllRecipiesCommandTest {

    @Test
    public void proccesGetAllRecipisFromRepository(){
        Repository repository = mock(Repository.class);
        Command command = new ViewAllRecipesCommand (repository);
        command.process(new HashMap<>());
        verify(repository.getAllRecipes());
    }

//    @Test
//    public void processReturnListRecipe() {
//        Repository repository = mock(Repository.class);
//        when(repository.getAllRecipes()).thenReturn(new ArrayList<Recipe>().add(new Recipe()));
//        Command command = new ViewAllRecipesCommand(repository);
//        command.process(new HashMap<>());
//        //assertThat procces return correctly filling ViewModel
//    }
}
