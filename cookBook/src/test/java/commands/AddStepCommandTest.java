package commands;

import by.javacourse.commands.Command;
import by.javacourse.commands.ViewTarget;
import mappers.RecipeMapper;
import org.junit.Before;
import org.junit.Test;
import recipe.CreateRecipeViewModel;
import recipe.Step;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AddStepCommandTest {
    private Map<String, String[]> parametrs;
    @Before
    public void prepareParametrsMap (){
        parametrs = new HashMap<>();
        parametrs.put("id", new String[]{"1"});
        parametrs.put("name", new String[]{"test"});
        parametrs.put("shortDescription", new String[]{"test"});
        parametrs.put("amountOfperson", new String[]{"1"});
        parametrs.put("addStep[name]", new String[]{"step1"});
        parametrs.put("addStep[description]", new String[]{"description"});
    }

    @Test
    public void shouldRerurnRecipeViewModelWithAddedStep() {
        Command command = new AddStepCommand();
        ViewTarget viewTarget = (ViewTarget) command.process(parametrs);
        RecipeMapper recipeMapper = new RecipeMapper();
        recipeMapper.setMap(parametrs);
        Step expectedAddStep = recipeMapper.getAddtStep();
        List<Step> steps = ((CreateRecipeViewModel) viewTarget.getParam()).getSteps();
        //verify that new step added to List<Step>
        assertThat(steps).contains(expectedAddStep);
    }
}
