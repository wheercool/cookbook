package by.javacourse;

import by.javacourse.commands.Command;
import by.javacourse.commands.ViewTarget;
import by.javacourse.configurations.Configuration;
import by.javacourse.controller.FrontController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FrontControllerTest {
    @InjectMocks
    FrontController frontController;
    @Mock
    Configuration configuration;
    @Mock
    ServletConfig servletConfig;

    private HttpServletRequest request;
    private HttpServletResponse response;
    private ServletContext servletContext;
    private RequestDispatcher requestDispatcher;

    @Before
    public void setUp(){

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        servletContext = mock(ServletContext.class);
        requestDispatcher = mock(RequestDispatcher.class);

    }

    @Test
    public void shouldThrowServletExceptionWhenDontGetAnyConfiguration(){
        try {
            when(servletConfig.getServletContext()).thenReturn(servletContext);
            when(servletContext.getInitParameter(anyString())).thenReturn("someFactoryName");
            frontController.init();
        } catch (Exception e) {
            assertThat(e).isInstanceOf(ServletException.class);
        }
    }

    @Test
    public void shouldReturnCommandFromConfiguration() throws ServletException, IOException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getRequestDispatcher(any())).thenReturn(requestDispatcher);
        final String commandName = "someCommand";
        when(request.getParameter(anyString())).thenReturn(commandName);
        Command someCommand = parametrs -> new ViewTarget("some.jsp", new Object());
        when(configuration.getCommand(commandName)).thenReturn(someCommand);
        //tested method
        frontController.service(request, response);
        verify(configuration).getCommand(commandName);


    }

    @Test
    public void shouldRequestContainsAttrubuiteObjectFromCommand () throws IllegalAccessException, ClassNotFoundException, InstantiationException, ServletException, IOException {
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getRequestDispatcher(any())).thenReturn(requestDispatcher);
        String commandName = "someCommand";
        Object model = new Object();
        when(request.getParameter(anyString())).thenReturn(commandName);
        Command someCommand = parametrs -> new ViewTarget("some.jsp", model);
        when(configuration.getCommand(commandName)).thenReturn(someCommand);
        frontController.service(request, response);
        verify(request).setAttribute("model", model);
    }
}
