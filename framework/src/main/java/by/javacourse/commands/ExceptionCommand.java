package by.javacourse.commands;

import java.util.Map;

/**
 * Created by puzevich on 18.10.16.
 */
public class ExceptionCommand implements Command {
    private Exception exception;

    public ExceptionCommand (Exception ex){
        exception = ex;
    }
    public Target process(Map<String, String[]> parametrs) {
        return new ViewTarget("/index.jsp", exception);
    }
}
