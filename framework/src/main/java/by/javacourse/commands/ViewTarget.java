package by.javacourse.commands;

/**
 * Created by puzevich on 03.10.16.
 */
public class ViewTarget extends Target {
     private Object param;

    public ViewTarget(String uri, Object param ) {
        super(uri);
        this.param = param;
    }

    public Object getParam() {
        return param;
    }

}
