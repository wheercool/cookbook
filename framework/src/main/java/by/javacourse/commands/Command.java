package by.javacourse.commands;

import java.util.Map;

/**
 * Created by puzevich on 30.09.16.
 */
public interface Command {
   public Target process(Map<String, String []> parametrs);
}
