package by.javacourse.commands;

/**
 * Created by puzevich on 03.10.16.
 */
public class Target {
    protected String uri;

    public Target(String uri){
        this.uri = uri;
    }
    public String getURI(){ return uri;}
}
