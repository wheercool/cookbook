package by.javacourse.configurations;

import by.javacourse.commands.Command;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


public class SpringConfiguration implements Configuration, ApplicationContextAware {
    private static Logger log = Logger.getLogger(SpringConfiguration.class);
    private ApplicationContext context;
    public SpringConfiguration () {
    }
    @Override
    public Command getCommand(String url) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        return (Command)context.getBean(url);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (context == null) {
            this.context = applicationContext;
            log.info("SET APPLICATION CONTEXT");
        }
    }
}
