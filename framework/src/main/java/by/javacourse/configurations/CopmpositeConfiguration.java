package by.javacourse.configurations;

import by.javacourse.commands.Command;

import java.util.ArrayList;

public class CopmpositeConfiguration implements Configuration {

    private ArrayList<Configuration> configurations = new ArrayList<>();
    @Override
    public Command getCommand(String url) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Command cmd = null;
        for (Configuration config : configurations) {
            cmd = config.getCommand(url);
            if (cmd != null){
                return cmd;
            }
        }
        return null;
    }

    public void add (Configuration configuration) {
        configurations.add(configuration);
    }
}
