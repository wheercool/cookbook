package by.javacourse.configurations;

import by.javacourse.commands.Command;

public interface Configuration {
        Command getCommand (String url) throws IllegalAccessException, InstantiationException, ClassNotFoundException;
}
