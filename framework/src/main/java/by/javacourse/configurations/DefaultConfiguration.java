package by.javacourse.configurations;

import by.javacourse.commands.Command;
import by.javacourse.commands.DefaultCommand;

/**
 * Created by puzevich on 18.10.16.
 */
public class DefaultConfiguration implements Configuration {
    public Command getCommand(String url) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        return new DefaultCommand();
    }

}
