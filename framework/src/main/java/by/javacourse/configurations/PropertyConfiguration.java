package by.javacourse.configurations;

import by.javacourse.commands.Command;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

public class PropertyConfiguration implements Configuration {
    private static Logger log = Logger.getLogger(PropertyConfiguration.class);
    private static final String DEFAULT_FILE_NAME = "framework.properties";
    private volatile Properties properties;

    public PropertyConfiguration () throws IOException {
            initProperties(DEFAULT_FILE_NAME);
            log.info("Read from file properties");
    }

    public PropertyConfiguration (String file_path) throws IOException {
        initProperties(file_path);
        log.info("Read from file properties");
    }

    public Command getCommand(String url) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
            Class result;
            final String commandClassName = properties.getProperty(url);
            result = Class.forName(commandClassName);
            return (Command) result.newInstance();
    }

    private void initProperties (String file_path) throws IOException {
            properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream(file_path));
    }
}
