package by.javacourse.factories;

import by.javacourse.configurations.Configuration;

/**
 * Created by puzevich on 21.10.16.
 */
public interface ConfigFactory {

   Configuration getConfiguration () throws Exception;

}
