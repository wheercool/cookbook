package by.javacourse.controller;

import by.javacourse.commands.Command;
import by.javacourse.commands.DefaultCommand;
import by.javacourse.commands.ViewTarget;
import by.javacourse.configurations.PropertyConfiguration;
import by.javacourse.configurations.SpringConfiguration;
import by.javacourse.factories.ConfigFactory;
import by.javacourse.commands.Target;
import by.javacourse.configurations.Configuration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.soap.Addressing;
import java.io.*;

@Component
public class FrontController extends HttpServlet {
    private static Logger log = Logger.getLogger(FrontController.class);
    private static final String NAME_CONFIG_FACTORY_PARAM = "configFactory";
    private Configuration configuration;
    private ApplicationContext webapp;

    @Override
    public void init() throws ServletException {
        super.init();
        log.info("---------initialization MY FRONT CONTROLLER-------------");
        final String config_factory = getServletContext().getInitParameter(NAME_CONFIG_FACTORY_PARAM);
        try {
           // configuration = new PropertyConfiguration();
            ConfigFactory configFactory = getCustomConfigFactory(config_factory);
            log.info(configFactory.getClass().getCanonicalName());
            configuration  = configFactory.getConfiguration();
            if (configuration instanceof SpringConfiguration) {
                ((SpringConfiguration) configuration).setApplicationContext(webapp);
                log.info("this is spring config");
            }
        } catch (Exception e) {
            log.error(e);
            throw new ServletException();
        }

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        try {
            doCommand(req, resp);
        } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
            log.error(e.getMessage(), e);
            resp.getWriter().print(e.getMessage());
        }
    }

    private void doCommand( HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        Command command = null;
        command = getCommand(req);
        Target target = command.process(req.getParameterMap());
        if (target instanceof ViewTarget) {
            req.setAttribute("model", ((ViewTarget) target).getParam());
        }
            getServletContext().getRequestDispatcher(target.getURI()).forward(req, resp);

    }

    private Command getCommand(HttpServletRequest req) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        String nameCommand = req.getParameter("command");

        return (nameCommand == null) ? new DefaultCommand() : configuration.getCommand(nameCommand);
    }

    private ConfigFactory getCustomConfigFactory (String classNameFactory) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ConfigFactory customConfig = null;
        customConfig = (ConfigFactory) Class.forName(classNameFactory).newInstance();
        return customConfig;
    }

    public void setApplicationContext (ApplicationContext context) {
        this.webapp = context;
        log.info("SET CONTEXT");
    }

}
